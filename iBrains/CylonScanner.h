//
//  CylonScanner.h
//  iBrains
//
//  Created by Antti Ainasoja on 02/03/2017.
//  Copyright © 2017 2A. All rights reserved.
//

#ifndef CylonScanner_h
#define CylonScanner_h

#import <UIKit/UIKit.h>

@interface CylonScanner: NSObject

-(id) init: (UIView*) view scanCycleDuration: (float) sdur;
-(void) Start;
-(void) Stop;

@end

#endif /* CylonScanner_h */
