//
//  CtrlCommandHandler.m
//  iBrains
//
//  Created by Antti Ainasoja on 16/08/16.
//  Copyright © 2016 2A. All rights reserved.
//

#import "CtrlCommandHandler.h"

#import "DoubleController.h"
#import "PeerServerHandler.h"
#import <OpenWebRTC-SDK/OpenWebRTC.h>

@interface CtrlCommandHandler ()
    //
@end


@implementation CtrlCommandHandler

DoubleController *dblCtrl;

- (id) initWithDoubleController:(DoubleController *) dblCtrlPtr;
{
    NSLog(@"[CtrlCommandHandler] Init msg handler.");
    dblCtrl = dblCtrlPtr;
    return [super init];
}

- (void) park
{
    [dblCtrl parkOn];
}

- (void) handleCommand:(NSArray *) cmdArray
{
    if ([cmdArray count] <= 0)
    {
        return;
    }
    NSString *cmd = cmdArray[0];
    NSLog(@"[cmdHandler] command: %@", cmd);
    
    // park on/off
    if ([cmd isEqual: @"park"])
    {
        if ([cmdArray count] > 1)
        {
            NSString *param = cmdArray[1];
            if ([param isEqual: @"off"])
            {
                [dblCtrl parkOff];
                return;
            }
        }
        [dblCtrl parkOn];
    }
    
    // driving
    if ([cmd isEqual: @"drive"])
    {
        NSLog(@"HANDLE DRIVE COMMAND");
        float v = 0.0;
        float t = 0.0;
        if ([cmdArray count] > 1)
        {
            NSString *param = cmdArray[1];
            v = [param doubleValue];
        }
        if ([cmdArray count] > 2)
        {
            NSString *param = cmdArray[2];
            t = [param doubleValue];
        }
        NSLog(@"Got speed: %f and turn %f", v, t);
        
        [dblCtrl drive:v turn:t];
    }
    
    
    // [dblCtrl togglePark];
}

@end
