//
//  Doubl3Controller.m
//  iBrains
//
//  Created by Antti Ainasoja on 15/08/16.
//  Copyright © 2016 2A. All rights reserved.
//

#import "DoubleController.h"
#import <DoubleControlSDK/DoubleControlSDK.h>

#import "PeerServerHandler.h"

@interface DoubleController () <DRDoubleDelegate>
    //
@end

@implementation DoubleController

PeerServerHandler *peerServer;
NSString *peerID;

float v_, t_;
bool drive;

int waitCount;
NSTimeInterval waitTime;

static float DELAY = 0.200; // s

float leftDistance;
float rightDistance;
float xDistance;
float yDistance;
float heading;

- (id) initWithPeerServer:(PeerServerHandler *) peerServerHandler
{
    v_ = 0.0;
    t_ = 0.0;
    drive = false;
    leftDistance = 0.0;
    rightDistance = 0.0;
    xDistance = 0.0;
    yDistance = 0.0;
    heading = 0.0;
    waitCount = 0;
    
    waitTime = 0.0;
    
    [DRDouble sharedDouble].delegate = self;
    peerServer = peerServerHandler;
    return [super init];
}

- (void) setPeer:(NSString *) peer
{
    peerID = peer;
}

- (void) parkOn
{
    drive = false;
    [[DRDouble sharedDouble] variableDrive:0 turn:0];
    [[DRDouble sharedDouble] deployKickstands];
    [[DRDouble sharedDouble] stopTravelData];
}

- (void) parkOff
{
    [[DRDouble sharedDouble] retractKickstands];
    [[DRDouble sharedDouble] startTravelData];
}

- (void) togglePark
{
    NSLog(@"Toggling parking.");
    standState parkState = [[DRDouble sharedDouble] kickstandState];
    switch (parkState) {
        case kDRKickstandStateDeployed:
        case kDRKickstandStateDeployingInProgress:
            [[DRDouble sharedDouble] retractKickstands];
            break;
        case kDRKickstandStateRetracted:
        case kDRKickstandStateRetractingInProgress:
            [[DRDouble sharedDouble] deployKickstands];
            break;
        case kDRKickstandStateUnknown:
             break;
    }
}

- (void) drive:(float) v turn: (float) t
{
    NSLog(@"Speed: %f, turn: %f.", v, t);
    v_= v;
    t_= t;
    drive = true;
    // [[DRDouble sharedDouble] drive:1 turn:t];
}

- (void) test
{
    NSLog(@"SDK Version: %@", kDoubleBasicSDKVersion);
    // NSLog(@"Double FW version: %@", [[DRDouble sharedDouble] firmwareVersion]);
    NSLog(@"[DoubleController] TEST");
}

#pragma mark - DRDoubleDelegate

- (void)doubleDidConnect:(DRDouble *)theDouble {
     NSLog(@"Connected");
}

- (void)doubleStatusDidUpdate:(DRDouble *)theDouble
{
    NSLog(@"pole h: %f", [DRDouble sharedDouble].poleHeightPercent);
    NSLog(@"kickstandstate: %d", [DRDouble sharedDouble].kickstandState);
    NSLog(@"battery: %f", [DRDouble sharedDouble].batteryPercent);
    NSLog(@"battery full: %d", [DRDouble sharedDouble].batteryIsFullyCharged);
    NSLog(@"FW ver: %@", [DRDouble sharedDouble].firmwareVersion);
    
    int ksState = [DRDouble sharedDouble].kickstandState;
    NSString *kssStr = [NSString stringWithFormat:@"%d", ksState];
    NSString *jsonStr = @"{\"data\": {\"status\": {\"stand_state\": \"&\"}}}";
    jsonStr = [jsonStr stringByReplacingOccurrencesOfString:@"&" withString:kssStr];
    if (peerID)
    {
        NSLog(@"Has peer");
        [peerServer sendMessage:jsonStr toPeer:peerID];
    }
}

- (void)doubleTravelDataDidUpdate:(DRDouble *)theDouble
{
    leftDistance  = leftDistance + [DRDouble sharedDouble].leftEncoderDeltaInches;
    rightDistance = rightDistance + [DRDouble sharedDouble].rightEncoderDeltaInches;
    xDistance  = xDistance + [DRDouble sharedDouble].xDeltaInches;
    yDistance = yDistance + [DRDouble sharedDouble].yDeltaInches;
    heading = heading + [DRDouble sharedDouble].headingDeltaRadians;
    waitCount = waitCount + 1;
    
    NSTimeInterval currentTime = [[NSDate date] timeIntervalSince1970];
    if (waitTime == 0.0) {
        waitTime = currentTime;
    }
    
    if (currentTime > waitTime + DELAY) {
        // compute speeds
        float speed = (xDistance*0.0254)/(currentTime-waitTime);
        
        NSString *lDistStr = [NSString stringWithFormat:@"%.3f", leftDistance];
        NSString *rDistStr = [NSString stringWithFormat:@"%.3f", rightDistance];
        NSString *xDistStr = [NSString stringWithFormat:@"%.3f", xDistance];
        NSString *yDistStr = [NSString stringWithFormat:@"%.3f", yDistance];
        NSString *headingStr = [NSString stringWithFormat:@"%.6f", heading];
        NSString *speedStr = [NSString stringWithFormat:@"%.6f", speed];
        
        NSString *jsonStr = @"{\"data\": {\"status\": {\"distance\": [&l&, &r&, &x&, &y&, &h&, &s&]}}}";
        jsonStr = [jsonStr stringByReplacingOccurrencesOfString:@"&l&" withString:lDistStr];
        jsonStr = [jsonStr stringByReplacingOccurrencesOfString:@"&r&" withString:rDistStr];
        jsonStr = [jsonStr stringByReplacingOccurrencesOfString:@"&x&" withString:xDistStr];
        jsonStr = [jsonStr stringByReplacingOccurrencesOfString:@"&y&" withString:yDistStr];
        jsonStr = [jsonStr stringByReplacingOccurrencesOfString:@"&h&" withString:headingStr];
        jsonStr = [jsonStr stringByReplacingOccurrencesOfString:@"&s&" withString:speedStr];
        if (peerID)
        {
            NSLog(@"Has peer");
            [peerServer sendMessage:jsonStr toPeer:peerID];
        }
        
        waitTime = currentTime;
        leftDistance = 0.0;
        rightDistance = 0.0;
        xDistance = 0.0;
        yDistance = 0.0;
        heading = 0.0;
        waitCount = 0;
    }
}


- (void)doubleDriveShouldUpdate:(DRDouble *)theDouble
{
    // NSLog(@"Drive should update!");
    if (drive)
    {
        [[DRDouble sharedDouble] variableDrive:v_ turn:t_];
        if (v_ == 0.0 && t_ == 0.0) {
            drive = false;
        }
    }
}


@end
