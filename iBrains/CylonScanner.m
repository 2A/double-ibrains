//
//  CylonScanner.m
//  iBrains
//
//  Created by Antti Ainasoja on 02/03/2017.
//  Copyright © 2017 2A. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "CylonScanner.h"

#import <UIKit/UIKit.h>
// #import <QuartzCore/QuartzCore.h>

@implementation CylonScanner : NSObject

UIView* parentView;
UIView* box;
CAKeyframeAnimation* trans;
CAKeyframeAnimation* mirr;

-(id) init: (UIView*) view scanCycleDuration: (float) sdur
{
    parentView = view;
    CGSize frameSize = parentView.frame.size;
    
    UIColor * bgRed = [UIColor colorWithRed:1.0 green:0.0 blue:0.0 alpha:0.0];

    box = [[UIView alloc] initWithFrame:CGRectMake(-50, 0, 50, frameSize.height)];
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = box.bounds;
 
    gradient.startPoint = CGPointMake(1.0, 0.5);
    gradient.endPoint = CGPointMake(0.0, 0.5);
    gradient.colors = @[(id)[UIColor redColor].CGColor, (id)bgRed.CGColor];
    [box.layer insertSublayer:gradient atIndex:0];
    
    // scan motion
    int endPt = frameSize.width + 50;
    trans = [CAKeyframeAnimation animationWithKeyPath:@"transform.translation.x"];
    NSArray *tvalues = @[@(0), @(endPt), @(endPt), @(0), @(0)];
    trans.values = tvalues;
    NSArray *ttimes = @[@(0.0), @(0.2), @(0.25), @(0.45), @(1.0)];
    trans.keyTimes = ttimes;
    trans.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
    trans.autoreverses = NO;
    trans.duration = sdur;
    trans.repeatCount = INFINITY;
   
    // Mirror scanline
    mirr = [CAKeyframeAnimation animationWithKeyPath:@"transform.scale.x"];
    NSArray *mvalues = @[@(1.0), @(-1.0)];
    NSArray *mtimes = @[@(0.22), @(0.23)];
    mirr.values = mvalues;
    mirr.keyTimes = mtimes;
    mirr.duration = sdur;
    mirr.autoreverses = NO;
    mirr.repeatCount = INFINITY;
    return [super init];
}

-(void) Start
{
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        [parentView addSubview:box];
        [box.layer addAnimation:trans  forKey:@"trans"];
        [box.layer addAnimation:mirr  forKey:@"mirr"];
    }];
}

-(void) Stop
{
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        [box.layer removeAllAnimations];
        [box removeFromSuperview];
    }];
}

@end
