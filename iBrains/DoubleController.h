//
//  DoubleController.h
//  iBrains
//
//  Created by Antti Ainasoja on 15/08/16.
//  Copyright © 2016 2A. All rights reserved.
//

#ifndef DoubleController_h
#define DoubleController_h

#import <Foundation/NSObject.h>

// # import "PeerServerHandler.h"
@class PeerServerHandler;

typedef enum kickstandStates {
    kDRKickstandStateUnknown = 0,
    kDRKickstandStateDeployed = 1,
    kDRKickstandStateRetracted = 2,
    kDRKickstandStateDeployingInProgress = 3,
    kDRKickstandStateRetractingInProgress = 4
} standState;

@interface DoubleController: NSObject

- (id) initWithPeerServer:(PeerServerHandler *) peerServerHandler;
- (void) setPeer:(NSString *) peer;
- (void) parkOn;
- (void) parkOff;
- (void) togglePark;
- (void) drive:(float) v turn: (float) t;
- (void) test;

@end

#endif /* DoubleController_h */
