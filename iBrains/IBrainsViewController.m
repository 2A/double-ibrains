//
//  NativeDemoViewController.m
//  NativeDemo
//
//  Copyright (c) 2015, Ericsson AB.
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without modification,
//  are permitted provided that the following conditions are met:
//
//  1. Redistributions of source code must retain the above copyright notice, this
//  list of conditions and the following disclaimer.
//
//  2. Redistributions in binary form must reproduce the above copyright notice, this
//  list of conditions and the following disclaimer in the documentation and/or other
//  materials provided with the distribution.

//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
//  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
//  IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
//  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
//  NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
//  PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
//  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
//  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
//  OF SUCH DAMAGE.
//

#import "IBrainsViewController.h"
#import "PeerServerHandler.h"
#import "VideoAttributes.h"

#import <OpenWebRTC-SDK/OpenWebRTC.h>

#import "DoubleController.h"

#import "CylonScanner.h"

// #define kServerURL @"http://2a.fi:8080"
// @"http://demo.openwebrtc.org"


@interface IBrainsViewController () <PeerServerHandlerDelegate, OpenWebRTCNativeHandlerDelegate>
{
    // IBOutlet UIBarButtonItem *callButton;
    // IBOutlet UIBarButtonItem *hangupButton;
    // IBOutlet UIBarButtonItem *parkButton;

    OpenWebRTCNativeHandler *nativeHandler;
    NSMutableArray *cameras;
    NSUInteger currentCameraIndex;
    
    DoubleController *dblCtrl;
    
    CylonScanner *cylon;
}

@property (weak) IBOutlet OpenWebRTCVideoView *selfView;
@property (weak) IBOutlet OpenWebRTCVideoView *remoteView;

@property (nonatomic, strong) NSString *roomID;
@property (nonatomic, strong) NSString *peerID;
@property (nonatomic, strong) PeerServerHandler *peerServer;


@property (weak, nonatomic) IBOutlet UIBarButtonItem *callButton;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *hangupButton;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *parkButton;

@end

@implementation IBrainsViewController

@synthesize callButton;
@synthesize hangupButton;
@synthesize parkButton;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // **** READ SERVER SETTINGS ****
    NSString *signserver;
    NSString *stunuri;
    int stunport = 5349;
    NSString *turnuri;
    int turnport = 5349;
    // signaling server
    signserver = [[NSUserDefaults standardUserDefaults] stringForKey:@"signal_server"];
    NSLog(@"Signaling server: %@", signserver);
    // stun server
    NSArray *stunserver = [[[NSUserDefaults standardUserDefaults] stringForKey:@"stun_server"] componentsSeparatedByString:@":"];
    if ([stunserver count] > 1) {
        stunuri = [NSString stringWithFormat:@"%@:%@", [stunserver objectAtIndex:0], [stunserver objectAtIndex:1] ];
    }
    if ([stunserver count] > 2) {
        stunport = [[stunserver objectAtIndex:2] intValue];
    }
    NSLog(@"STUN server: %@, port: %i", stunuri, stunport);
    // turn server
    NSArray *turnserver = [[[NSUserDefaults standardUserDefaults] stringForKey:@"turn_server"] componentsSeparatedByString:@":"];
    if ([turnserver count] > 1) {
        turnuri = [NSString stringWithFormat:@"%@:%@", [turnserver objectAtIndex:0], [turnserver objectAtIndex:1] ];
    }
    if ([turnserver count] > 2) {
        turnport = [[turnserver objectAtIndex:2] intValue];
    }
    NSLog(@"TURN server: %@, port: %i", turnuri, turnport);
    NSString *turnuser = [[NSUserDefaults standardUserDefaults] stringForKey:@"turn_user"];
    NSString *turnpass = [[NSUserDefaults standardUserDefaults] stringForKey:@"turn_password"];
    
    // **** read done
    
    
    [self.navigationController setToolbarHidden:NO animated:YES];

    nativeHandler = [[OpenWebRTCNativeHandler alloc] initWithDelegate:self];

    // Setup the video windows.
    self.selfView.hidden = YES;
    [nativeHandler setSelfView:self.selfView];
    [nativeHandler setRemoteView:self.remoteView];
    
    [nativeHandler addSTUNServerWithAddress:stunuri
                                       port:stunport];
    [nativeHandler addTURNServerWithAddress:turnuri
                                       port:turnport
                                   username:turnuser
                                   password:turnpass
                                      isTCP:NO];
    callButton.enabled = NO;
    hangupButton.enabled = NO;
    
    self.peerServer = [[PeerServerHandler alloc] initWithBaseURL:signserver];
    self.peerServer.delegate = self;

    // Configure OpenWebRTC with media settings.
    VideoAttributes *attrs = [VideoAttributes loadFromSettings];
    NSLog(@"Video settings: %@", attrs);

    OpenWebRTCSettings *settings = [[OpenWebRTCSettings alloc] initWithDefaults];
    settings.videoFramerate = attrs.framerate;
    settings.videoBitrate = (int)attrs.bitrate;
    settings.videoWidth = (int)attrs.width;
    settings.videoHeight = (int)attrs.height;
    nativeHandler.settings = settings;

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(sendCurrentOrientation)
                                                 name:UIDeviceOrientationDidChangeNotification
                                               object:nil];
    
    dblCtrl = [[DoubleController alloc] initWithPeerServer:self.peerServer];
    [self.peerServer setDoubleController:dblCtrl];
    
    cylon = [[CylonScanner alloc] init:self.view scanCycleDuration:5.0];
}

- (void)viewDidAppear:(BOOL)animated
{
    // NSNumber *value = [NSNumber numberWithInt:UIInterfaceOrientationPortraitUpsideDown];
    
    // [UIView setAnimationsEnabled:YES];
    [self presentRoomInputView];
}

- (void)presentRoomInputView
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Enter Session ID"
                                                                   message:@"Enter Session ID"
                                                            preferredStyle:UIAlertControllerStyleAlert];

    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"Done"
                                                 style:UIAlertActionStyleDefault
                                               handler:^(UIAlertAction * action) {
                                                   UITextField *roomTextField = alert.textFields[0];
                                                   NSString *room = [roomTextField text];
                                                   if (![@"" isEqualToString:room])
                                                       [self joinButtonTapped:room];
                                                   else
                                                       [self presentRoomInputView];
                                               }];
    [alert addAction:ok];
    [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.keyboardType = UIKeyboardTypeNumberPad;
    }];

    [self presentViewController:alert animated:YES completion:nil];
}

- (void)joinButtonTapped:(NSString *)roomID
{
    NSLog(@"Joining room with ID: %@", roomID);
    self.roomID = roomID;

    [nativeHandler startGetCaptureSourcesForAudio:YES video:YES];

    NSString *deviceID = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
    
    [self.peerServer joinRoom:roomID withDeviceID:deviceID];
}

- (IBAction)parkButtonTapped:(id)sender
{
    NSLog(@"parkButtonTapped");
    [dblCtrl parkOn];
}

- (IBAction)callButtonTapped:(id)sender
{
    NSLog(@"callButtonTapped");

    callButton.enabled = NO;
    hangupButton.enabled = YES;

    [nativeHandler initiateCall];
}

- (IBAction)hangupButtonTapped:(id)sender
{
    NSLog(@"hangupButtonTapped");

    [nativeHandler terminateCall];
    [self.peerServer leave];

    callButton.enabled = NO;
    hangupButton.enabled = NO;

    [self presentRoomInputView];
    // [cylon Stop];
}

- (IBAction)selfViewTapped:(id)sender
{
    return; // camera switching doesnt work atm
    if ([cameras count] < 2) {
        NSLog(@"WARNING! Camera switching needs atleast 2 cameras to work");
        return;
    }

    currentCameraIndex = currentCameraIndex + 1;
    if ([cameras count] <= currentCameraIndex)
    {
        currentCameraIndex = 0;
    }
    
    [nativeHandler setVideoCaptureSourceByName:cameras[currentCameraIndex]];

    [UIView transitionWithView:self.selfView
                      duration:0.6
                       options:UIViewAnimationOptionTransitionFlipFromTop
                    animations:nil
                    completion:nil];
}

- (void)didReceiveMemoryWarning
{
    NSLog(@"WARNING! didReceiveMemoryWarning");
    [super didReceiveMemoryWarning];
}

- (void)presentErrorWithMessage:(NSString *)message
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error!"
                                                                   message:message
                                                            preferredStyle:UIAlertControllerStyleAlert];

    //UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK"
    //                                             style:UIAlertActionStyleDefault
    //                                           handler:nil];
    
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK"
                                                 style:UIAlertActionStyleDefault
                                               handler:^(UIAlertAction * action) {
                                                   [self presentRoomInputView];
                                               }];
    
    [alert addAction:ok];
    [self presentViewController:alert animated:YES completion:nil];
}

#pragma mark - OpenWebRTCNativeHandlerDelegate

- (void)answerGenerated:(NSDictionary *)answer
{
    NSLog(@"Answer generated: \n%@", answer);

    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:answer
                                                       options:0
                                                         error:nil];
    NSString *answerString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];

    if (self.peerID) {
        [self.peerServer sendMessage:answerString toPeer:self.peerID];
    }
}

- (void)offerGenerated:(NSDictionary *)offer
{
    NSLog(@"Offer generated: \n%@", offer);

    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:offer
                                                       options:0
                                                         error:nil];
    NSString *offerString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];

    if (self.peerID) {
        [self.peerServer sendMessage:offerString toPeer:self.peerID];
    }
}

- (void)candidateGenerated:(NSDictionary *)candidate
{
    NSLog(@"Candidate generated: \n%@", candidate);
    if (self.peerID) {
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:@{@"candidate": candidate}
                                                           options:0
                                                             error:nil];
        NSString *candidateJson = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        NSLog(@"Sending candidate to peer: %@", self.peerID);
        [self.peerServer sendMessage:candidateJson toPeer:self.peerID];
    }
}

- (void)gotLocalSources:(NSArray *)sources
{
    NSLog(@"gotLocalSources: %@", sources);

    cameras = [NSMutableArray arrayWithCapacity:[sources count]];
    for (NSDictionary *source in sources) {
        if ([source[@"mediaType"] isEqualToString:@"video"]) {
            [cameras addObject:source[@"name"]];
        }
    }
    NSLog(@"Found cameras: %@", cameras);
    currentCameraIndex = 0;

    [nativeHandler videoView:self.selfView setVideoRotation:0];
    self.selfView.hidden = NO;
}

- (void)gotRemoteSource:(NSDictionary *)source
{
    NSLog(@"gotRemoteSource: %@", source);
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        callButton.enabled = NO;
        hangupButton.enabled = YES;
    }];
    
    [self sendCurrentOrientation];
    
    [cylon Start];
}

- (void)sendCurrentOrientation
{
    NSInteger orientation;
    switch ([[UIDevice currentDevice] orientation]) {
        case UIDeviceOrientationLandscapeLeft:
            orientation = 180;
            break;
        case UIDeviceOrientationLandscapeRight:
            orientation = 0;
            break;
        case UIDeviceOrientationPortrait:
            orientation = 90;
            break;
        case UIDeviceOrientationPortraitUpsideDown:
            orientation = 270;
            break;
        default:
            orientation = 0;
            break;
    };

    [nativeHandler videoView:self.selfView setVideoRotation:orientation - 90];

    NSString *message = [NSString stringWithFormat:@"{\"orientation\": %ld}", (long)orientation];
    if (self.peerID) {
        NSLog(@"[PeerServerHandler] Sending orientation msg: %@", message);
        [self.peerServer sendMessage:message toPeer:self.peerID];
    }
}

#pragma mark - PeerServerHandlerDelegate

- (void)peerServer:(PeerServerHandler *)peerServer failedToJoinRoom:(NSString *)roomID withError:(NSError *)error
{
    NSLog(@"NSURLErrorDomain: %@", NSURLErrorDomain);
    NSLog(@"Error domain: %@", [error domain]);
    
    if ([[error domain] isEqualToString:NSURLErrorDomain]) {
        [self presentErrorWithMessage:@"Error connecting to server."];
        // [self presentRoomInputView];
    }
    else {
        [self presentErrorWithMessage:error.description];
    }
}

- (void)peerServer:(PeerServerHandler *)peerServer roomIsFull:(NSString *)roomID
{
    NSLog(@"roomIsFull: %@", roomID);
}

- (void)peerServer:(PeerServerHandler *)peerServer peer:(NSString *)peerID joinedRoom:(NSString *)roomID
{
    NSLog(@"peer <%@> joinedRoom: %@", peerID, roomID);

    callButton.enabled = YES;
    hangupButton.enabled = NO;
    self.peerID = peerID;
    [dblCtrl setPeer:self.peerID];

    [self sendCurrentOrientation];
}

- (void)peerServer:(PeerServerHandler *)peerServer peer:(NSString *)peerID leftRoom:(NSString *)roomID
{
    NSLog(@"peer <%@> leftRoom: %@", peerID, roomID);
    self.peerID = nil;
    [dblCtrl parkOn];
    [dblCtrl setPeer:nil];
    [cylon Stop];
}

- (void)peerServer:(PeerServerHandler *)peerServer peer:(NSString *)peerID sentOffer:(NSDictionary *)offer
{
    NSLog(@"peer <%@> sentOffer: %@", peerID, offer);
    [nativeHandler handleOfferReceived:offer];
}

- (void)peerServer:(PeerServerHandler *)peerServer peer:(NSString *)peerID sentAnswer:(NSDictionary *)answer
{
    NSLog(@"peer <%@> sentAnswer: %@", peerID, answer);
    [nativeHandler handleAnswerReceived:answer];
}

- (void)peerServer:(PeerServerHandler *)peerServer peer:(NSString *)peerID sentCandidate:(NSDictionary *)candidate
{
    NSLog(@"peer <%@> sentCandidate: %@", peerID, candidate);
    [nativeHandler handleRemoteCandidateReceived:candidate];
}

- (void)peerServer:(PeerServerHandler *)peerServer peer:(NSString *)peerID sentOrientation:(NSInteger)orientation
{
    NSLog(@"Rotating remote view to: %ld", (long)orientation);
    [nativeHandler videoView:self.remoteView setVideoRotation:orientation];
}

- (void)peerServer:(PeerServerHandler *)peerServer failedToSendDataWithError:(NSError *)error
{
    [self presentErrorWithMessage:error.description];
}

@end
