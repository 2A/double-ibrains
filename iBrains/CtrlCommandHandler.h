//
//  CtrlMessageHandler.h
//  iBrains
//
//  Created by Antti Ainasoja on 16/08/16.
//  Copyright © 2016 2A. All rights reserved.
//

#ifndef CtrlCommandHandler_h
#define CtrlCommandHandler_h

#import <Foundation/NSObject.h>

#import "DoubleController.h"

@interface CtrlCommandHandler: NSObject

- (id) initWithDoubleController:(DoubleController *) dblCtrlPtr;
- (void) handleCommand:(NSArray *) cmdArray;
- (void) park;

@end

#endif /* CtrlCommandHandler_h */
