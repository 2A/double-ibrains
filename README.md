# iOS-application for controlling a Double robot

[Double](http://www.doublerobotics.com/) robot "brain" applications to be used with [server](https://bitbucket.org/2A/double-server) and [Android-client](https://bitbucket.org/2A/double-droidmote).

Based on [OpenWebRTC NativeDemo for iOS ](https://github.com/EricssonResearch/openwebrtc-examples/tree/master/ios/NativeDemo).

[Double Robotics Basic Control SDK iOS](https://github.com/doublerobotics/Basic-Control-SDK-iOS) is used for communication with the Double base.

## Installation

The app uses the `OpenWebRTC` and `OpenWebRTC-SDK` CocoaPods from [https://github.com/EricssonResearch/openwebrtc-ios-sdk](https://github.com/EricssonResearch/openwebrtc-ios-sdk). Since `OpenWebRTC-SDK` is (still) a local podspec file you need to clone `openwebrtc-ios-sdk` before you can run NativeDemo.

If you haven't installed CocoaPods yet, do so first:

    sudo gem install cocoapods
    pod setup

Then run:

    pod install
    open NativeDemo.xcworkspace

**NOTE!** When using CocoaPods, you should always use the `.xcworkspace` file and not the usual `.xcproject`.
